import java.util.*;

/**
 * Created by Artem on 2017-04-12.
 */
public class Disk {
    int size,max,head,headtemp,block,difference,previous,next;
    int sum=0;
    ArrayList<Integer> queue=new ArrayList<>();
    ArrayList<Integer> tempq=new ArrayList<>();
    ArrayList<Process> processes=new ArrayList<>();

    public void create(){
        Scanner sc=new Scanner(System.in);
        System.out.print("Enter the max range of disk:\n");
        max=sc.nextInt();
        System.out.print("Enter the size of queue request:\n");
        size=sc.nextInt();
        System.out.print("Enter the queue:\n");
        for(int i=0;i<size-1;i++){
            block=sc.nextInt();
            queue.add(block);
            tempq.add(block);
            Process process=new Process(block,0);
            processes.add(process);
        }
        System.out.print("Enter the initial head position:\n");
        head=sc.nextInt();
        headtemp=head;
        queue.add(0,head);
        tempq.add(0,head);
        Process process=new Process(head,0);
        processes.add(process);
    }
    void FCFS(){
        System.out.print("FCFS\n");
        for(int i=0;i<queue.size()-1;i++){
            int a=queue.get(i);
            int b=queue.get(i+1);
            difference=Math.abs(a-b);
            sum+=difference;
            System.out.printf("Move is from %d to %d with seek %d\n",a,b,difference);
        }
        System.out.printf("FCSF Total seek time is %d\n",sum);

    }

    void SSTF() {
        System.out.println("\nSSTF");
        sum=0;
        ArrayList<Integer> temp = new ArrayList<>();
        while(queue.size()!=1) {
            for (int j = 1; j < queue.size(); j++) {
                temp.add(Math.abs(head - queue.get(j)));
            }
            int k = 1;
            int min = temp.get(0);
            for (; k < temp.size(); k++) {
                if (min > temp.get(k)){
                    min = temp.get(k);
                }
            }
            sum += min;
            int q=temp.indexOf(min);
            System.out.printf("Move is from %d to %d with seek %d\n",head,queue.get(q+1),min);
            head = queue.get(q+1);
            queue.remove(q+1);
            temp.clear();
        }
        System.out.printf("SSTF Total seek time is %d\n", sum);
    }

    void SCAN(){
        System.out.println("\nSCAN");
        sum=0;
        for(int i=1;i<tempq.size();i++){
            for(int j=i;j>0 && tempq.get(j-1)>tempq.get(j);j--){
                int temp=tempq.get(j);
                tempq.set(j,tempq.get(j-1));
                tempq.set(j-1,temp);
            }
        }
        tempq.add(0,0);
        int headPos=tempq.indexOf(headtemp);
        for(int i=headPos;i>0;i--){
            previous=tempq.get(i-1);
            next=tempq.get(i);
            difference= Math.abs(next-previous);
            sum+=difference;
            System.out.printf("Move is from %d to %d with seek %d\n",next,previous,difference);
        }
        if(headPos<tempq.size()-1) {
            int headPosnext = tempq.get(headPos + 1);
            sum += headPosnext;
            System.out.printf("Move is from %d to %d with seek %d\n", 0, headPosnext, headPosnext);
            for (int j = headPos + 1; j < tempq.size() - 1; j++) {
                previous = tempq.get(j);
                next = tempq.get(j + 1);
                difference = Math.abs(previous - next);
                sum += difference;
                System.out.printf("Move is from %d to %d with seek %d\n", previous, next, difference);
            }
        }
        System.out.printf("SCAN Total seek time is %d\n", sum);
    }

    void CSCAN(){
        tempq.add(max);
        System.out.println("\nCSCAN");
        sum=0;
        int headPos=tempq.indexOf(headtemp);
        for(int i=headPos;i>0;i--){
            previous=tempq.get(i-1);
            next=tempq.get(i);
            difference= Math.abs(next-previous);
            sum+=difference;
            System.out.printf("Move is from %d to %d with seek %d\n",next,previous,difference);
        }

        System.out.printf("Move is from %d to %d with seek %d\n",0,max,0);
        for(int j=tempq.size()-1;j>headPos+1;j--){
            previous=tempq.get(j);
            next=tempq.get(j-1);
            difference=Math.abs(previous-next);
            sum+=difference;
            System.out.printf("Move is from %d to %d with seek %d\n",previous,next,difference);
        }
        System.out.printf("CSCAN Total seek time is %d\n", sum);
    }

    void EDF(){
        System.out.println("\nEDF");
        sum=0;
        Scanner sc=new Scanner(System.in);
        System.out.print("Enter the deadlines:\n");
        for(int i=0;i<size;i++){
            int deadline=sc.nextInt();
            processes.get(i).setDeadline(deadline);
        }
        processes.sort(new Comparator<Process>() {
            @Override
            public int compare(Process process, Process t1) {
                return process.getDeadline() > t1.getDeadline()? 1 : t1.getDeadline() > process.getDeadline()? -1 : 0;
            }
        });
        int j=0;
        for(int i=1;i<processes.size();i++){
            previous=processes.get(j).getRequest();
            next=processes.get(i).getRequest();
            difference=Math.abs(previous-next);
            sum+=difference;
            if((processes.get(i).getDeadline())>=sum){
                j=i;
                System.out.printf("Move is from %d to %d with seek %d\n",previous,next,difference);
            }
            else sum-=difference;
        }
        System.out.printf("EDF Total seek time is %d\n", sum);
    }

    void FDSCAN(){
        System.out.println("\nFDSCAN");
        sum=0;
        int j=0;
        for(int i=1;i<processes.size();i++){
            previous=processes.get(j).getRequest();
            next=processes.get(i).getRequest();
            difference=Math.abs(previous-next);
            sum+=difference;
            if((processes.get(i).getDeadline())>=sum){
                j=i;
                System.out.printf("Move is from %d to %d with seek %d\n",previous,next,difference);
                for(int k=0;k<processes.size();k++){
                    int request=processes.get(k).getRequest();
                    if((request < previous && request > next) || (request > previous && request < next)){
                        processes.remove(processes.get(k));
                        k--;
                    }
                }
            }
            else sum-=difference;
        }
        System.out.printf("FD-SCAN Total seek time is %d\n", sum);
    }
    public static void main(String[] arg){
        Disk disc=new Disk();
        disc.create();
        disc.FCFS();
        disc.SSTF();
        disc.SCAN();
        disc.CSCAN();
        disc.EDF();
        disc.FDSCAN();
    }
}
