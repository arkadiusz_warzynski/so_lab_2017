/**
 * Created by Artem on 2017-04-13.
 */
public class Process {
    private int request;
    private int deadline;

    public Process(int request,int deadline){
        this.request=request;
        this.deadline=deadline;
    }

    public int getRequest() {
        return request;
    }

    public void setRequest(int request) {
        this.request = request;
    }

    public int getDeadline() {
        return deadline;
    }

    public void setDeadline(int deadline) {
        this.deadline = deadline;
    }
}
